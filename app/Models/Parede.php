<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parede extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['altura', 'largura', 'metro_quadrado', 'porta', 'janela', 'sala_id'];
}
