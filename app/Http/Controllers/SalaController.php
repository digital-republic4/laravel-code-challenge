<?php

namespace App\Http\Controllers;

use App\Models\Sala;
use App\Models\Parede;
use Illuminate\Http\Request;

class SalaController extends Controller
{
    public function index() {
        $salas = Sala::all();
        return view('sala', ['salas' => $salas]);
    }

    public function create(Sala $sala) {
        $metros_total = 0;
        $litros3 = 0;
        $litros2 = 0;
        $litros05 =0;
        $paredes = Parede::all()->where('sala_id', $sala->id);
        foreach ($paredes as $parede) {
            $metros_total = $metros_total + $parede['metro_quadrado'];
        }
        
        $sala->metros_total = $metros_total;
        $sala->save();
        if($metros_total >= 18){
            $litros18 = intdiv($metros_total,18);

            $resto = $metros_total - (18 * $litros18);
            
            if($resto >= 3){
                if($resto <= 3.6){
                    $litros3 = 1;
                }else{
                    $litros3 = intdiv($resto, 3.6);
                    $resto= $resto - (3.6 * $litros3);
                    if($resto != 0){
                        $litros05 = 1;
                    }
                }
            }else if($resto >= 2){
                if($resto <= 2.5){
                    $litros2 = 1;
                }else {
                    $litros2 = intdiv($resto, 2.5);
                    $resto = $resto - (2.5 * $litros2);
                    if($resto != 0){
                        $litros05 = 1;
                    }
                }
                
            }else{
                if($resto != 0){
                
                $litros05 = intdiv($resto, 0.5);
                $resto = $resto - (0.5 * $litros05);  
                }
            }
        }

        $sala->litros = $litros18 . " latas de 18 L, " . $litros3 . " latas de 3,6 L, " . $litros2 . " latas de 2,5L, " . $litros05 . " latas de 0,5 L";
        $sala->save();
        
        return view('sala', ['salas' => [$sala]]);
    }

    public function store(Request $request) {
        
    }
}
