<?php

namespace App\Http\Controllers;

use App\Models\Parede;
use App\Models\Sala;
use Illuminate\Http\Request;

class ParedeController extends Controller
{
    public function create() {
        return view('criar');
    }

    public function store(Request $request) {
        $sala = Sala::create();
        for ($i=1; $i <= 4; $i++) { 
            $parede = Parede::create([
                'altura' => $request->input('altura-'.$i),
                'largura' => $request->input('largura-'.$i),
                'metro_quadrado' => $request->input('metro_quadrado-'.$i),
                'porta' => $request->input('porta-'.$i),
                'janela' => $request->input('janela-'.$i),
                'sala_id' => $sala->id
            ]);
            
            
        }

        return redirect()->route('create', $sala->id);
    }
}
