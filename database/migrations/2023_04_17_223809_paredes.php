<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('paredes', function (Blueprint $table) {
            $table->id();
            $table->double('altura');
            $table->double('largura');
            $table->double('metro_quadrado');
            $table->boolean('porta');
            $table->boolean('janela');
            $table->foreignId('sala_id')->constrained('salas');
            
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('paredes');
    }
};
