<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.6.4.js" integrity="sha256-a9jBBRygX1Bh5lt8GZjXDzyOB+bWve9EiO7tROUtj/E=" crossorigin="anonymous"></script>

<script src="{{asset('/js/index.js')}}"></script>

    <title>Code Challenge - Criar Sala</title>
</head>
<body>
    <div class="container">
        <div class="botoes">
            <a href="{{route('index')}}" class="button">Voltar</a>
            
        </div>
        <div class="conteudo">
            <p id="erro"></p>
            <form action="" id="formulario">
                
                <label for="largura">Largura</label>
                <input type="text" name="largura" id="largura">

                <label for="altura">Altura</label>
                <input type="text" name="altura" id="altura">
                <div class="margin">
                <label for="porta">Porta</label>
                <input type="checkbox" name="porta" id="porta" value="1">

                <label for="janela">Janela</label>
                <input type="checkbox" name="janela" id="janela" value="1">
                </div>
                <button type="button" class="button" id="criar">Criar Parede</button>
            </form>
                
            <form action="{{route('storeParede')}}" id="resultado" method="POST">
                @csrf
                <button type="submit" class="button" disabled id="salvar">Salvar Nova Parede</button>
            </form>
        </div>
    </div>
</body>
</html>