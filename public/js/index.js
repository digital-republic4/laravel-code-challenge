$(document).ready(function () {
    var count = 0;
    $('#criar').on('click', function(){
        $('#erro').text(' ');
        console.log('clicou')
        var erro = false;
        var largura = $('#largura').val();
        var altura = $('#altura').val();
        var metro = largura * altura;
        var metroFinal = metro;
        if($('#janela').prop('checked')){
            var valorJanela = 2.00 * 1.20;
            metroFinal = metroFinal - valorJanela  
        }
        if($('#porta').prop('checked') && $('#janela').prop('checked')){
            var valorPorta = 0.80 * 1.90;
            var portaJanela = valorJanela + valorPorta
        }
        if($('#porta').prop('checked')){
            var valorPorta = 0.80 * 1.90;
            metroFinal = metroFinal - valorPorta  
                if(altura - 1.90 <= 0.30){
                    erro = true;
                    $('#erro').text('A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta (1.90 Metros)')
                }
        } 
        if(metro >= 1 && metro <= 50 && erro == false){
            if(metro * 0.5 < valorPorta || metro * 0.5 < valorJanela || metro * 0.5 < portaJanela){
                $('#erro').text('O total de área deve ser no máximo 50% da área da parede');
            }else{
                $('#largura').val('');
                $('#altura').val('');
                $('#porta').prop('checked', false);
                $('#janela').prop('checked', false);
                count++;
                $('#resultado').append("<h3>Parede nº "+count+" OK");
                $('#resultado').append("<input type='hidden' name='largura-"+count+"' value='"+largura+"' />")
                $('#resultado').append("<input type='hidden' name='altura-"+count+"' value='"+altura+"' />")
                $('#resultado').append("<input type='hidden' name='metro_quadrado-"+count+"' value='"+metroFinal+"' />")
                if($('#porta').prop('checked')){
                    $('#resultado').append("<input type='hidden' name='porta-"+count+"' value='1' />")
                }else{
                    $('#resultado').append("<input type='hidden' name='porta-"+count+"' value='0' />")
                }
                if($('#janela').prop('checked')){
                    $('#resultado').append("<input type='hidden' name='janela-"+count+"' value='1' />")
                }else{
                    $('#resultado').append("<input type='hidden' name='janela-"+count+"' value='0' />")
                }
                if(count == 4){
                    $('#criar').attr('disabled', 'disabled')
                    $('#salvar').removeAttr('disabled')
                }
            }
        }
    })

    
});