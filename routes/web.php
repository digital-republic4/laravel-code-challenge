<?php

use App\Http\Controllers\SalaController;
use App\Http\Controllers\ParedeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [SalaController::class, 'index'])->name('index');
Route::get('/create/{sala}', [SalaController::class, 'create'])->name('create');
Route::get('/parede/create', [ParedeController::class, 'create'])->name('createParede');
Route::post('/parede/store', [ParedeController::class, 'store'])->name('storeParede');
